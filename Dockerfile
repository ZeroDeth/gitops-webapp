FROM scratch

LABEL maintaner="ZeroDeth"

COPY . .

EXPOSE 8080

CMD ["./main"]
